# terraform-gitlab-projects

A Terraform module to create GitLab projects.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 15.10.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | 15.10.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_branch_protection.default](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection) | resource |
| [gitlab_branch_protection.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection) | resource |
| [gitlab_project.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project) | resource |
| [gitlab_project_environment.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_environment) | resource |
| [gitlab_project_protected_environment.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_protected_environment) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_approvals_before_merge"></a> [approvals\_before\_merge](#input\_approvals\_before\_merge) | (Optional) Number of merge request approvals required for merging. Default is 0. | `string` | `1` | no |
| <a name="input_branches_to_protect"></a> [branches\_to\_protect](#input\_branches\_to\_protect) | List of branches or wildcards to protect | `list(any)` | `[]` | no |
| <a name="input_container_expiration_policy"></a> [container\_expiration\_policy](#input\_container\_expiration\_policy) | Object of container expiration policy. `cadence` controls the interval at which cleanup will run: `1d`, `7d`, `14d`, `1month`, `3month`. Use `keep_n` to keep `n` number of images: 1, 5, 10, 25, 50, or 100 . Use `older_than` to specify a number of days: 7d, 14d, 30d, 60d, or 90d - images older than this will be cleaned up. | <pre>object({<br>    cadence    = string<br>    keep_n     = number<br>    older_than = string<br>  })</pre> | <pre>{<br>  "cadence": "1month",<br>  "keep_n": 25,<br>  "older_than": "30d"<br>}</pre> | no |
| <a name="input_container_registry_access_level"></a> [container\_registry\_access\_level](#input\_container\_registry\_access\_level) | Whether the GitLab Container Registry is enabled for this project. One of `enabled`, `disabled`, or `private`. Defaults to `disabled`. | `string` | `"disabled"` | no |
| <a name="input_default_branch"></a> [default\_branch](#input\_default\_branch) | (Optional) The default branch the repository will use. Defaults to main. | `string` | `"main"` | no |
| <a name="input_default_branch_code_owner_approval_required"></a> [default\_branch\_code\_owner\_approval\_required](#input\_default\_branch\_code\_owner\_approval\_required) | Whether codeowner approval is required for the default branch. Defaults to `false`. | `bool` | `false` | no |
| <a name="input_default_branch_push_access"></a> [default\_branch\_push\_access](#input\_default\_branch\_push\_access) | Which access level can push to the default branch. Defaults to `maintainer`. | `string` | `"maintainer"` | no |
| <a name="input_description"></a> [description](#input\_description) | (Optional) A description of the project. | `string` | `""` | no |
| <a name="input_developers_can_deploy"></a> [developers\_can\_deploy](#input\_developers\_can\_deploy) | Whether `developer`s can run CI jobs. Defaults to `false` (`maintainer`s only). | `bool` | `false` | no |
| <a name="input_environments"></a> [environments](#input\_environments) | List of environments for this project | `list(any)` | `[]` | no |
| <a name="input_environments_to_protect"></a> [environments\_to\_protect](#input\_environments\_to\_protect) | List of CI environments to protect | `list(any)` | `[]` | no |
| <a name="input_initialize_with_readme"></a> [initialize\_with\_readme](#input\_initialize\_with\_readme) | (Optional) Create main branch with first commit containing a README.md file. | `bool` | `true` | no |
| <a name="input_lfs_enabled"></a> [lfs\_enabled](#input\_lfs\_enabled) | (Optional) Enable LFS for the project. | `bool` | `false` | no |
| <a name="input_merge_method"></a> [merge\_method](#input\_merge\_method) | (Optional) Set to `ff` to create fast-forward merges. Valid values are `merge`, `rebase_merge`, `ff`. | `string` | `"ff"` | no |
| <a name="input_name"></a> [name](#input\_name) | The name of the project to be created | `string` | n/a | yes |
| <a name="input_namespace_id"></a> [namespace\_id](#input\_namespace\_id) | The ID of the namespace in which to create the project. | `number` | n/a | yes |
| <a name="input_only_allow_merge_if_all_discussions_are_resolved"></a> [only\_allow\_merge\_if\_all\_discussions\_are\_resolved](#input\_only\_allow\_merge\_if\_all\_discussions\_are\_resolved) | (Optional) Set to true if you want to allow merges only if all discussions are resolved. | `bool` | `true` | no |
| <a name="input_only_allow_merge_if_pipeline_succeeds"></a> [only\_allow\_merge\_if\_pipeline\_succeeds](#input\_only\_allow\_merge\_if\_pipeline\_succeeds) | (Optional) Set to true if you want to allow merges only if a pipeline succeeds. | `bool` | `true` | no |
| <a name="input_packages_enabled"></a> [packages\_enabled](#input\_packages\_enabled) | (Optional) Enable packages repository for the project. | `bool` | `false` | no |
| <a name="input_pages_access_level"></a> [pages\_access\_level](#input\_pages\_access\_level) | (Optional) Enable pages access control. Valid values are `disabled`, `private`, `enabled`, `public`. | `string` | `"private"` | no |
| <a name="input_push_rules"></a> [push\_rules](#input\_push\_rules) | An array containing the push rules object. | `any` | <pre>[<br>  {<br>    "commit_committer_check": true,<br>    "prevent_secrets": true<br>  }<br>]</pre> | no |
| <a name="input_remove_source_branch_after_merge"></a> [remove\_source\_branch\_after\_merge](#input\_remove\_source\_branch\_after\_merge) | (Optional) Enable `Delete source branch` option by default for all new merge requests. | `bool` | `true` | no |
| <a name="input_request_access_enabled"></a> [request\_access\_enabled](#input\_request\_access\_enabled) | (Optional) Allow users to request member access. | `bool` | `true` | no |
| <a name="input_snippets_enabled"></a> [snippets\_enabled](#input\_snippets\_enabled) | (Optional) Enable snippets for the project. | `bool` | `false` | no |
| <a name="input_visibility_level"></a> [visibility\_level](#input\_visibility\_level) | (Optional) Set to `public` to create a public project. Valid values are `private`, `internal`, `public`. | `string` | `"private"` | no |
| <a name="input_wiki_enabled"></a> [wiki\_enabled](#input\_wiki\_enabled) | (Optional) Enable wiki for the project. | `bool` | `false` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
