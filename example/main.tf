terraform {
  required_version = ">= 1.0.0"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 15.10.0"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_token
}

data "gitlab_group" "this" {
  full_path = "TransformUK"
}

module "project_test" {
  source = "../"

  name         = "test-project"
  namespace_id = data.gitlab_group.this.group_id
}
