variable "gitlab_token" {
  description = "GitLab Access Token."
  type        = string
  sensitive   = true
}
