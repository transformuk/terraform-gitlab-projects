resource "gitlab_project" "this" {
  name         = var.name
  description  = var.description
  namespace_id = var.namespace_id

  visibility_level                = var.visibility_level
  default_branch                  = var.default_branch
  initialize_with_readme          = var.initialize_with_readme
  container_registry_access_level = var.container_registry_access_level

  request_access_enabled = var.request_access_enabled
  lfs_enabled            = var.lfs_enabled
  packages_enabled       = var.packages_enabled
  snippets_enabled       = var.snippets_enabled
  wiki_enabled           = var.wiki_enabled

  merge_method                                     = var.merge_method
  approvals_before_merge                           = var.approvals_before_merge
  only_allow_merge_if_pipeline_succeeds            = var.only_allow_merge_if_pipeline_succeeds
  only_allow_merge_if_all_discussions_are_resolved = var.only_allow_merge_if_all_discussions_are_resolved
  remove_source_branch_after_merge                 = var.remove_source_branch_after_merge

  pages_access_level = var.pages_access_level

  dynamic "container_expiration_policy" {
    for_each = var.container_registry_access_level != "disabled" ? [var.container_expiration_policy] : []
    iterator = policy
    content {
      enabled    = true
      cadence    = try(policy.value.cadence, null) # 1d, 7d, 14d, 1month, 3month
      keep_n     = try(policy.value.keep_n, null)
      older_than = try(policy.value.older_than, null)
    }
  }

  dynamic "push_rules" {
    for_each = length(var.push_rules) > 0 ? var.push_rules : []
    content {
      author_email_regex            = try(push_rules.value.author_email_regex, null)
      branch_name_regex             = try(push_rules.value.branch_name_regex, null)
      commit_committer_check        = try(push_rules.value.commit_committer_check, null)
      commit_message_negative_regex = try(push_rules.value.commit_message_negative_regex, null)
      commit_message_regex          = try(push_rules.value.commit_message_regex, null)
      deny_delete_tag               = try(push_rules.value.deny_delete_tag, null)
      file_name_regex               = try(push_rules.value.file_name_regex, null)
      max_file_size                 = try(push_rules.value.max_file_size, null)
      member_check                  = try(push_rules.value.member_check, null)
      prevent_secrets               = try(push_rules.value.prevent_secrets, null)
      reject_unsigned_commits       = try(push_rules.value.reject_unsigned_commits, null)
    }
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "gitlab_branch_protection" "default" {
  project                      = gitlab_project.this.id
  branch                       = var.default_branch
  push_access_level            = var.default_branch_push_access
  merge_access_level           = "developer"
  unprotect_access_level       = "maintainer"
  allow_force_push             = false
  code_owner_approval_required = var.default_branch_code_owner_approval_required
}

resource "gitlab_branch_protection" "this" {
  for_each = toset(var.branches_to_protect)

  project                      = gitlab_project.this.id
  branch                       = each.key
  push_access_level            = "developer"
  merge_access_level           = "developer"
  unprotect_access_level       = "maintainer"
  allow_force_push             = true
  code_owner_approval_required = false
}

resource "gitlab_project_environment" "this" {
  for_each = toset(var.environments)

  name    = each.key
  project = gitlab_project.this.id
}

resource "gitlab_project_protected_environment" "this" {
  for_each = toset(var.environments_to_protect)

  project     = gitlab_project.this.id
  environment = each.key

  deploy_access_levels {
    access_level = var.developers_can_deploy == true ? "developer" : "maintainer"
  }
}
