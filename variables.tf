variable "name" {
  type        = string
  description = "The name of the project to be created"

  validation {
    condition     = can(regex("^[a-z0-9-]*$", var.name))
    error_message = "Projects must be named in lowercase, using a-z, 0-9, and - (hyphen) symbols only."
  }
}

variable "approvals_before_merge" {
  type        = string
  description = "(Optional) Number of merge request approvals required for merging. Default is 0."
  default     = 1
}

variable "default_branch" {
  type        = string
  description = "(Optional) The default branch the repository will use. Defaults to main."
  default     = "main"
}

variable "description" {
  type        = string
  description = "(Optional) A description of the project."
  default     = ""
}

variable "initialize_with_readme" {
  type        = bool
  description = "(Optional) Create main branch with first commit containing a README.md file."
  default     = true
}

variable "lfs_enabled" {
  type        = bool
  description = "(Optional) Enable LFS for the project."
  default     = false
}

variable "merge_method" {
  type        = string
  description = "(Optional) Set to `ff` to create fast-forward merges. Valid values are `merge`, `rebase_merge`, `ff`."
  default     = "ff"
}

variable "only_allow_merge_if_all_discussions_are_resolved" {
  type        = bool
  description = "(Optional) Set to true if you want to allow merges only if all discussions are resolved."
  default     = true
}

variable "only_allow_merge_if_pipeline_succeeds" {
  type        = bool
  description = "(Optional) Set to true if you want to allow merges only if a pipeline succeeds."
  default     = true
}

variable "packages_enabled" {
  type        = bool
  description = "(Optional) Enable packages repository for the project."
  default     = false
}

variable "pages_access_level" {
  type        = string
  description = "(Optional) Enable pages access control. Valid values are `disabled`, `private`, `enabled`, `public`."
  default     = "private"
}

variable "remove_source_branch_after_merge" {
  type        = bool
  description = "(Optional) Enable `Delete source branch` option by default for all new merge requests."
  default     = true
}

variable "request_access_enabled" {
  type        = bool
  description = "(Optional) Allow users to request member access."
  default     = true
}

variable "snippets_enabled" {
  type        = bool
  description = "(Optional) Enable snippets for the project."
  default     = false
}

variable "visibility_level" {
  type        = string
  description = "(Optional) Set to `public` to create a public project. Valid values are `private`, `internal`, `public`."
  default     = "private"
}

variable "wiki_enabled" {
  type        = bool
  description = "(Optional) Enable wiki for the project."
  default     = false
}

variable "push_rules" {
  description = "An array containing the push rules object."
  type        = any
  default = [{
    commit_committer_check = true
    prevent_secrets        = true
  }]
}

variable "namespace_id" {
  description = "The ID of the namespace in which to create the project."
  type        = number
}

variable "branches_to_protect" {
  description = "List of branches or wildcards to protect"
  type        = list(any)
  default     = []
}

variable "environments" {
  description = "List of environments for this project"
  type        = list(any)
  default     = []
}

variable "environments_to_protect" {
  description = "List of CI environments to protect"
  type        = list(any)
  default     = []
}

variable "developers_can_deploy" {
  description = "Whether `developer`s can run CI jobs. Defaults to `false` (`maintainer`s only)."
  type        = bool
  default     = false
}

variable "container_registry_access_level" {
  description = "Whether the GitLab Container Registry is enabled for this project. One of `enabled`, `disabled`, or `private`. Defaults to `disabled`."
  type        = string
  default     = "disabled"
}

variable "container_expiration_policy" {
  description = "Object of container expiration policy. `cadence` controls the interval at which cleanup will run: `1d`, `7d`, `14d`, `1month`, `3month`. Use `keep_n` to keep `n` number of images: 1, 5, 10, 25, 50, or 100 . Use `older_than` to specify a number of days: 7d, 14d, 30d, 60d, or 90d - images older than this will be cleaned up."

  type = object({
    cadence    = string
    keep_n     = number
    older_than = string
  })

  default = {
    cadence    = "1month"
    keep_n     = 25
    older_than = "30d"
  }

  validation {
    condition     = contains(["1d", "7d", "14d", "1month", "3month"], var.container_expiration_policy["cadence"])
    error_message = "Cadence must be one of `1d`, `7d`, `14d`, `1month`, or `3month`."
  }

  validation {
    condition     = contains([1, 5, 10, 25, 50, 100], var.container_expiration_policy["keep_n"])
    error_message = "keep_n must be one of 1, 5, 10, 25, 50, or 100."
  }

  validation {
    condition     = contains(["7d", "14d", "30d", "60d", "90d"], var.container_expiration_policy["older_than"])
    error_message = "older_than must be one of `7d`, `14d`, `30d`, `60d`, or `90d`."
  }
}

variable "default_branch_push_access" {
  description = "Which access level can push to the default branch. Defaults to `maintainer`."
  type        = string
  default     = "maintainer"
}

variable "default_branch_code_owner_approval_required" {
  description = "Whether codeowner approval is required for the default branch. Defaults to `false`."
  type        = bool
  default     = false
}
